/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.interfaces.core.InterfaceServiceService;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a los servicios de
 * interfaces.
 *
 * @author MSTN Media
 */
@Path("/interfaceService")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.INTERFACES_LIST})
public class InterfaceServiceResource {

	private final InterfaceServiceService service;

	/**
	 *
	 * @param service
	 */
	public InterfaceServiceResource(InterfaceServiceService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Interface_Service>> getAll(@Auth User user, @QueryParam("where") String where) {
		try {
			WhereClause whereClause = new WhereClause(where);
			List<Interface_Service> list = service.getAll(whereClause);
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Interface_Service>> get(@Auth User user, @PathParam("id") final int id) {
		Interface_Service item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.INTERFACES_ADD})
	public Representation<List<Interface_Service>> create(@Auth User user, @NotNull @Valid Interface_Service item) {
		try {
			List<Interface_Service> list = new ArrayList<>();
			list.add(service.create(item, user));
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.INTERFACES_EDIT})
	public Representation<List<Interface_Service>> edit(
			@Auth User user,
			@NotNull @Valid Interface_Service item,
			@PathParam("id") final int id) throws Exception {
		try {
			item.setId(id);
			List<Interface_Service> list = new ArrayList<>();
			list.add(service.update(item, user));
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.INTERFACES_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		try {
			return new Representation(HttpStatus.OK_200, service.delete(id, user));
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getService")
	public Interface_Service getService(@Auth User user, @NotNull @Valid final IdValueParams params) {
		return service.get(params.getId());
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getServices")
	public List<Interface_Service> getServices(@Auth User user, @NotNull @Valid final IdValueParams params) {
		try {
			WhereClause whereClause = new WhereClause(params.getValue());
			List<Interface_Service> list = service.getAll(whereClause, params.getId() == 1);
			return list;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}
}
