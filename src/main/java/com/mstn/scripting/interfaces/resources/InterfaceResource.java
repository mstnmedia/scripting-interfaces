/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.interfaces.core.InterfaceService;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a interfaces
 * estáticas, aquellas que heredan de la clase
 * {@link com.mstn.scripting.core.models.InterfaceConnectorBase}.
 *
 * @author MSTN Media
 */
@Path("/interface")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.INTERFACES_LIST})
public final class InterfaceResource {

	private final InterfaceService service;

	/**
	 *
	 * @param service
	 */
	public InterfaceResource(InterfaceService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Interface>> getAll(@Auth User user) throws Exception {
		List<Interface> list = service.getAll();
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Interface>> get(@Auth User user, @PathParam("id") final int id) throws Exception {
		List<Interface> list = new ArrayList<>();
		list.add(service.get(id, true));
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param interfaceID
	 * @param strPayload
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/{id}/getForm")
	@RolesAllowed({UserPermissions.INTERFACES_CONSULT})
	public TransactionInterfacesResponse getForm(
			@Auth User user,
			@PathParam("id") final int interfaceID,
			@QueryParam("payload") String strPayload) throws Exception {
		TransactionInterfacesPayload payload = JSON.toObject(strPayload, TransactionInterfacesPayload.class);
		return getForm(user, interfaceID, payload);
	}

	/**
	 *
	 * @param user
	 * @param interfaceID
	 * @param payload
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/{id}/getForm")
	@RolesAllowed({UserPermissions.INTERFACES_CONSULT})
	public TransactionInterfacesResponse getForm(
			@Auth User user,
			@PathParam("id") final int interfaceID,
			@NotNull @Valid final TransactionInterfacesPayload payload) throws Exception {
		TransactionInterfacesResponse response = service.getForm(interfaceID, payload, user);
		return response;
	}

	/**
	 *
	 * @param user
	 * @param payload
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/getForm")
	@RolesAllowed({UserPermissions.INTERFACES_CONSULT})
	public TransactionInterfacesResponse getForm(
			@Auth User user,
			@NotNull @Valid final TransactionInterfacesPayload payload
	) throws Exception {
		int interfaceID = payload.getWorkflow_step().getId_workflow_external();
		return getForm(user, interfaceID, payload);
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/getInterface")
	public Interface getInterface(@Auth User user, @NotNull @Valid final IdValueParams params) throws Exception {
		boolean loadChildren = Boolean.parseBoolean(params.getValue());
		return service.get(params.getId(), loadChildren);
	}

}
