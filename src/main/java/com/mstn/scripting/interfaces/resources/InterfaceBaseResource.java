/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.interfaces.core.InterfaceBaseService;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a interfaces
 * dinámicas.
 *
 * @author MSTN Media
 */
@Path("/interfaceBase")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.INTERFACES_LIST})
public final class InterfaceBaseResource {

	private final InterfaceBaseService service;

	/**
	 *
	 * @param service
	 */
	public InterfaceBaseResource(InterfaceBaseService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Interface>> getAll(@Auth User user) throws Exception {
		try {
			List<Interface> list = service.getAll();
			return new Representation<>(HttpStatus.OK_200, list);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Interface>> get(@Auth User user, @PathParam("id") final int id) throws Exception {
		Interface item = service.get(id, true);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param interfaceID
	 * @param strPayload
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/{id}/exec")
	@RolesAllowed({UserPermissions.INTERFACES_CONSULT})
	public TransactionInterfacesResponse getExec(
			@Auth User user,
			@PathParam("id") final int interfaceID,
			@QueryParam("payload") String strPayload) throws Exception {
		try {
			TransactionInterfacesPayload payload = JSON.toObject(strPayload, TransactionInterfacesPayload.class);
			return postExec(user, interfaceID, payload);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param interfaceID
	 * @param payload
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/{id}/exec")
	@RolesAllowed({UserPermissions.INTERFACES_CONSULT})
	public TransactionInterfacesResponse postExec(
			@Auth User user,
			@PathParam("id") final int interfaceID,
			@NotNull @Valid final TransactionInterfacesPayload payload) throws Exception {
		try {
			TransactionInterfacesResponse response = service.exec(interfaceID, payload, user);
			return response;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param payload
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/exec")
	@RolesAllowed({UserPermissions.INTERFACES_CONSULT})
	public TransactionInterfacesResponse exec(
			@Auth User user,
			@NotNull @Valid final TransactionInterfacesPayload payload
	) throws Exception {
		try {
			int interfaceID = payload.getWorkflow_step().getId_workflow_external();
			return postExec(user, interfaceID, payload);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/getInterface")
	public Interface getInterface(@Auth User user, @NotNull @Valid final IdValueParams params) throws Exception {
		try {
			boolean loadChildren = Boolean.parseBoolean(params.getValue());
			return service.get(params.getId(), loadChildren);
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

}
