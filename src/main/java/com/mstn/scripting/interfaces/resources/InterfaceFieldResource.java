/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.interfaces.core.InterfaceFieldService;
import com.mstn.scripting.core.models.Interface_Field;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a las campos de
 * interfaces.
 *
 * @author MSTN Media
 * @deprecated
 */
@Path("/interfaceField")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.INTERFACES_LIST})
public class InterfaceFieldResource {

	private final InterfaceFieldService service;

	/**
	 *
	 * @param service
	 */
	public InterfaceFieldResource(InterfaceFieldService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Interface_Field>> getAll(@Auth User user, @QueryParam("where") String where) {
		WhereClause whereClause = new WhereClause(where);
		List<Interface_Field> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Interface_Field>> get(@Auth User user, @PathParam("id") final int id) {
		List<Interface_Field> list = new ArrayList<>();
		list.add(service.get(id));
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.INTERFACES_ADD})
	public Representation<List<Interface_Field>> create(@Auth User user, @NotNull @Valid final Interface_Field item) {
		List<Interface_Field> list = new ArrayList<>();
		list.add(service.create(item));
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.INTERFACES_EDIT})
	public Representation<List<Interface_Field>> edit(@Auth User user, @NotNull @Valid final Interface_Field item,
			@PathParam("id") final int id) {
		item.setId(id);

		List<Interface_Field> list = new ArrayList<>();
		list.add(service.update(item));
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.INTERFACES_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id));
	}
}
