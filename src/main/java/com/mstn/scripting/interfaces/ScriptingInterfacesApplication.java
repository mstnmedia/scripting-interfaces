package com.mstn.scripting.interfaces;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.AuthFilterUtils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.ScriptingInterfacesConfiguration;
import com.mstn.scripting.interfaces.core.InterfaceBaseService;
import com.mstn.scripting.interfaces.core.InterfaceResultService;
import com.mstn.scripting.interfaces.core.InterfaceService;
import com.mstn.scripting.interfaces.core.InterfaceServiceService;
import com.mstn.scripting.interfaces.health.ScriptingInterfacesHealthCheck;
import com.mstn.scripting.interfaces.resources.InterfaceBaseResource;
import com.mstn.scripting.interfaces.resources.InterfaceResource;
import com.mstn.scripting.interfaces.resources.InterfaceResultResource;
import com.mstn.scripting.interfaces.resources.InterfaceServiceResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.PolymorphicAuthDynamicFeature;
import io.dropwizard.auth.PolymorphicAuthValueFactoryProvider;
import io.dropwizard.auth.PrincipalImpl;
import io.dropwizard.auth.basic.BasicCredentials;
import io.dropwizard.client.HttpClientBuilder;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.bundles.DBIExceptionsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.apache.http.client.HttpClient;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.jose4j.jwt.consumer.JwtContext;
import org.simplejavamail.mailer.Mailer;
import org.skife.jdbi.v2.DBI;

/**
 * Clase ejecutable que inicia el servidor y registra los componentes necesarios
 * para el funcionamiento del microservicio Interfaces.
 *
 * @author MSTN Media
 */
public class ScriptingInterfacesApplication extends Application<ScriptingInterfacesConfiguration> {

	static final String INTERFACE_SERVICE = "Interface service";

	final List<Class> connectorClasses = new ArrayList();
	final List<InterfaceConnectorBase> interfacesConnectors = new ArrayList();

	final List<Class> baseClasses = new ArrayList();
	final List<InterfaceBase> interfaceBases = new ArrayList();

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		try {
			new ScriptingInterfacesApplication().run(args);
		} catch (Exception ex) {
			Utils.logException(ScriptingInterfacesApplication.class, ex.getClass().getName());
			Utils.logException(ScriptingInterfacesApplication.class, ex.getMessage(), ex);
		}
	}

	@Override
	public String getName() {
		return "ScriptingInterfaces";
	}

	@Override
	public void initialize(final Bootstrap<ScriptingInterfacesConfiguration> bootstrap) {
		bootstrap.addBundle(new DBIExceptionsBundle());
	}

	@Override
	public void run(final ScriptingInterfacesConfiguration config, final Environment environment) throws Exception {
		try {
			TimeZone.setDefault(TimeZone.getTimeZone(config.getTimeZone()));
			Locale.setDefault(Locale.forLanguageTag(config.getLocale()));
			registerResources(config, environment);
			registerAuthFilters(environment);
		} catch (Exception ex) {
			Utils.logException(ScriptingInterfacesApplication.class, ex.getClass().getName());
			Utils.logException(ScriptingInterfacesApplication.class, ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param config
	 */
	public void readJarsFolder(ScriptingInterfacesConfiguration config) {
		try {
			String jarsFolder = config.getJarsFolder();
			File folder = new File(jarsFolder);
			File[] files = folder.listFiles();
			for (File file : files) {
				String filePath = file.getAbsolutePath();
				if (filePath.endsWith(".jar")) {
					loadClassesFromJar(filePath);
				}
			}
			baseClasses.sort(Comparator.comparing(Class::getName, String.CASE_INSENSITIVE_ORDER));
			connectorClasses.sort(Comparator.comparing(Class::getName, String.CASE_INSENSITIVE_ORDER));
		} catch (Exception ex) {
			exitWithException(ex);
		}
	}

	/**
	 *
	 * @param pathToJar
	 * @throws Exception
	 */
	public void loadClassesFromJar(String pathToJar) throws Exception {
		Utils.logInfo(this.getClass(), "\n\nReading .jar file in {0}", pathToJar);
		JarFile jarFile = new JarFile(pathToJar);
		Enumeration<JarEntry> e = jarFile.entries();

		URL[] urls = {new URL("jar:file:" + pathToJar + "!/")};
		URLClassLoader cl = URLClassLoader.newInstance(urls);

		while (e.hasMoreElements()) {
			JarEntry je = e.nextElement();
			String className = je.getName()
					.replace('/', '.')
					.replace(".class", "");
			if (je.isDirectory()
					|| !je.getName().endsWith(".class")
					|| !className.startsWith("com.mstn.scripting.interfaces")
					|| false) {
				continue;
			}
			Utils.logDebug(this.getClass(), className);
			Class loadedClass = cl.loadClass(className);

			if (InterfaceBase.class.isAssignableFrom(loadedClass)) {
				baseClasses.add(loadedClass);
			} else if (InterfaceConnectorBase.class.isAssignableFrom(loadedClass)) {
				connectorClasses.add(loadedClass);
			}
		}
		Utils.logDebug(this.getClass(), "\n\n");
	}

	private void registerResources(final ScriptingInterfacesConfiguration config, final Environment environment) throws Exception {
		final DBIFactory factory = new DBIFactory();
		final DBI dbi = factory.build(environment, config.getDataSourceFactory(), "database");

		final HttpClient httpClient = new HttpClientBuilder(environment)
				.using(config.getHttpClientConfiguration())
				.build(getName());
		final Mailer mailer = config.getMailServer().build(environment, "mailer");

		InterfaceConfigs.microserviceConfig = config;
		InterfaceConfigs.showLogs = config.getShowInterfaceLogs();
		InterfaceConfigs.configs = config.getInterfaces();
		InterfaceConfigs.dbi = dbi;
		InterfaceConfigs.httpClient = httpClient;
		InterfaceConfigs.mailer = mailer;
		InterfaceConfigs.configs.put("mailFromName", config.getMailFromName());
		InterfaceConfigs.configs.put("mailFromEmail", config.getMailFromEmail());

		readJarsFolder(config);
		loadInterfaceConnectors();
		loadInterfaceBases();

		InterfaceResultService interfaceResultService = dbi.onDemand(InterfaceResultService.class);
		InterfaceServiceService interfaceServiceService = dbi.onDemand(InterfaceServiceService.class)
				.setHttpClient(httpClient)
				.setConfig(config);
		InterfaceService interfaceService = new InterfaceService()
				.setInterfaceServiceService(interfaceServiceService)
				.setConnectors(interfacesConnectors);
		InterfaceBaseService interfaceBaseService = dbi.onDemand(InterfaceBaseService.class)
				.setInterfaceResultService(interfaceResultService)
				.setInterfaceServiceService(interfaceServiceService)
				.setInterfaceBases(interfaceBases);

		ScriptingInterfacesHealthCheck healthCheck = new ScriptingInterfacesHealthCheck(interfaceService);
		environment.healthChecks().register(INTERFACE_SERVICE, healthCheck);
		environment.jersey().register(new InterfaceResource(interfaceService));
		environment.jersey().register(new InterfaceBaseResource(interfaceBaseService));
		environment.jersey().register(new InterfaceServiceResource(interfaceServiceService));
		environment.jersey().register(new InterfaceResultResource(interfaceResultService));
	}

	/**
	 *
	 */
	public void loadInterfaceConnectors() {
		try {
			Utils.logInfo(this.getClass(), "\n\n Instancing InterfaceConnectorBase classes");
			for (int i = 0; i < connectorClasses.size(); i++) {
				Class loadedClass = connectorClasses.get(i);
				Utils.logDebug(this.getClass(), loadedClass.getCanonicalName());
				InterfaceConnectorBase connector = (InterfaceConnectorBase) loadedClass.newInstance();
				connector.test();
				// TODO: Validar que no devuelve dos campos con el mismo nombre.
				// TODO: Validar que no devuelve dos resultados con el mismo nombre.
				// TODO: Validar que dos interfaces no tienen el mismo ID
				interfacesConnectors.add(connector);
			}
			HashMap<String, InterfaceConnectorBase> mapInterfaces = new HashMap();
			for (int i = 0; i < interfacesConnectors.size(); i++) {
				InterfaceConnectorBase connector = interfacesConnectors.get(i);
				boolean duplicated = mapInterfaces.containsKey(String.valueOf(connector.getID()))
						|| mapInterfaces.containsKey(connector.getNAME());
				if (duplicated) {
					throw new Exception("Duplicated " + connector.getNAME() + " " + connector.getID());
				} else {
					mapInterfaces.put(String.valueOf(connector.getID()), connector);
					mapInterfaces.put(connector.getNAME(), connector);
				}
			}
			Utils.logDebug(this.getClass(), "\n\n");
		} catch (Exception ex) {
			exitWithException(ex);
		}
	}

	/**
	 *
	 */
	public void loadInterfaceBases() {
		try {
			Utils.logInfo(this.getClass(), "\n\n Instancing InterfaceBase classes");
			for (int i = 0; i < baseClasses.size(); i++) {
				Class loadedClass = baseClasses.get(i);
				Utils.logDebug(this.getClass(), loadedClass.getCanonicalName());
				InterfaceBase base = (InterfaceBase) loadedClass.newInstance();
				base.test();
				interfaceBases.add(base);
			}
			HashMap<String, InterfaceBase> mapInterfaces = new HashMap();
			for (int i = 0; i < interfaceBases.size(); i++) {
				InterfaceBase connector = interfaceBases.get(i);
				boolean duplicated = mapInterfaces.containsKey(connector.getNAME());
				if (duplicated) {
					throw new Exception("Duplicated " + connector.getNAME() + " " + connector.getID());
				} else {
					mapInterfaces.put(connector.getNAME(), connector);
				}
			}
			Utils.logDebug(this.getClass(), "\n\n");
		} catch (Exception ex) {
			exitWithException(ex);
		}
	}

	/**
	 *
	 * @param ex
	 */
	public void exitWithException(Exception ex) {
		Utils.logException(this.getClass(), ex.getMessage(), ex);
		System.exit(1);
	}

	private void registerAuthFilters(Environment environment) {
		AuthFilterUtils authFilterUtils = new AuthFilterUtils();
		final AuthFilter<BasicCredentials, PrincipalImpl> basicFilter = authFilterUtils.buildBasicAuthFilter();
		final AuthFilter<JwtContext, User> jwtFilter = authFilterUtils.buildJwtAuthFilter();

		final PolymorphicAuthDynamicFeature feature = new PolymorphicAuthDynamicFeature<>(
				ImmutableMap.of(PrincipalImpl.class, basicFilter, User.class, jwtFilter));
		final AbstractBinder binder = new PolymorphicAuthValueFactoryProvider.Binder<>(
				ImmutableSet.of(PrincipalImpl.class, User.class));

		environment.jersey().register(feature);
		environment.jersey().register(binder);
		environment.jersey().register(RolesAllowedDynamicFeature.class);
	}
}
