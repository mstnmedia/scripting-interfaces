/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.db;

import com.mstn.scripting.core.models.Interface_Result;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code interface_result}.
 *
 * @author josesuero
 * @deprecated 
 */
@RegisterMapper(Interface_ResultDao.Mapper.class)
@UseStringTemplate3StatementLocator
public interface Interface_ResultDao {

	/**
	 *
	 */
	static public class Mapper implements ResultSetMapper<Interface_Result> {

		private static final String ID = "id";
		private static final String ID_INTERFACE = "id_interface";
		private static final String NAME = "name";
		private static final String LABEL = "label";
		private static final String PROPS = "props";

		@Override
		public Interface_Result map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Interface_Result(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_INTERFACE) ? resultSet.getInt(ID_INTERFACE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(LABEL) ? resultSet.getString(LABEL) : "",
					hasColumn.containsKey(PROPS) ? resultSet.getString(PROPS) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from interface_result <where> order by label")
	List<Interface_Result> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_interface
	 * @return
	 */
	@SqlQuery("select * from interface_result where id_interface = :id_interface order by label")
	List<Interface_Result> getAll(@Bind("id_interface") final int id_interface);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from interface_result where id = :id")
	Interface_Result get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into interface_result(id, id_interface, name, label, props)"
			+ "	values(interface_result_seq.nextVal, :id_interface, :name, :label, :props)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Interface_Result item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update interface_result set "
			+ " id_interface = :id_interface,"
			+ " name = :name,"
			+ " label = :label,"
			+ " props = :props "
			+ "where id = :id")
	void update(@BindBean final Interface_Result item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from interface_result where id = :id")
	int delete(@Bind("id") final int id);
}
