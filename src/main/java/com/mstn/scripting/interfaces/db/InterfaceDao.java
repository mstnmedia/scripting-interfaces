/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.db;

import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code interface}.
 *
 * @author josesuero
 */
@RegisterMapper(InterfaceDao.Mapper.class)
@UseStringTemplate3StatementLocator
public interface InterfaceDao {

	/**
	 *
	 */
	static public class Mapper implements ResultSetMapper<Interface> {

		private static final String ID = "id";
		private static final String NAME = "name";
		private static final String LABEL = "label";
		private static final String SERVER_URL = "server_url";
		private static final String VALUE_STEP = "value_step";
		private static final String KEEP_RESULTS = "keep_results";
		private static final String DELETED = "deleted";

		@Override
		public Interface map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Interface(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(LABEL) ? resultSet.getString(LABEL) : "",
					hasColumn.containsKey(SERVER_URL) ? resultSet.getString(SERVER_URL) : "",
					hasColumn.containsKey(VALUE_STEP) ? resultSet.getString(VALUE_STEP) : "",
					hasColumn.containsKey(KEEP_RESULTS) ? resultSet.getBoolean(KEEP_RESULTS) : Boolean.FALSE,
					hasColumn.containsKey(DELETED) ? resultSet.getBoolean(DELETED) : Boolean.FALSE
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from interface <where> order by label")
	List<Interface> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from interface where id = :id")
	Interface get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into interface(id, name, label, server_url, value_step, keep_results, deleted) "
			+ " values(interface_seq.nextVal, :name, :label, :server_url, :value_step, :keep_results, :deleted)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Interface item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update interface set "
			+ " name= :name, "
			+ " label= :label, "
			+ " server_url= :server_url, "
			+ " value_step= :value_step, "
			+ " keep_results= :keep_results, "
			+ " deleted= :deleted "
			+ "where id = :id")
	void update(@BindBean final Interface item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from interface where id = :id")
	int delete(@Bind("id") final int id);
}
