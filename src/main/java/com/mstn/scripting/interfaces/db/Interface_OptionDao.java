/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.db;

import com.mstn.scripting.core.models.Interface_Option;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code interface_option}.
 *
 * @author josesuero
 * @deprecated
 */
@RegisterMapper(Interface_OptionDao.Mapper.class)
@UseStringTemplate3StatementLocator
public interface Interface_OptionDao {

	/**
	 *
	 */
	static public class Mapper implements ResultSetMapper<Interface_Option> {

		private static final String ID = "id";
		private static final String ID_INTERFACE = "id_interface";
		private static final String NAME = "name";
		private static final String ID_TYPE = "id_type";
		private static final String VALUE = "value";
		private static final String COLOR = "color";
		private static final String ORDER_INDEX = "order_index";

		@Override
		public Interface_Option map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Interface_Option(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_INTERFACE) ? resultSet.getInt(ID_INTERFACE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(ID_TYPE) ? resultSet.getInt(ID_TYPE) : 0,
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : "",
					hasColumn.containsKey(COLOR) ? resultSet.getString(COLOR) : "",
					hasColumn.containsKey(ORDER_INDEX) ? resultSet.getInt(ORDER_INDEX) : 0
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from interface_option <where>")
	List<Interface_Option> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_interface
	 * @return
	 */
	@SqlQuery("select * from interface_option where id_interface = :id_interface order by order_index, id")
	List<Interface_Option> getAll(@Bind("id_interface") final int id_interface);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from interface_option where id = :id")
	Interface_Option get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into interface_option(id, id_interface, name, id_type, value, color, order_index) values(interface_option_seq.nextVal, :id_interface, :name, :id_type, :value, :color, :order_index)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Interface_Option item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update interface_option set "
			+ " id_interface = :id_interface, "
			+ " name = :name, "
			+ " id_type = :id_type, "
			+ " value = :value, "
			+ " color = :color, "
			+ " order_index = :order_index "
			+ "where id = :id")
	void update(@BindBean final Interface_Option item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from interface_option where id = :id")
	int delete(@Bind("id") final int id);
}
