/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.db;

import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code interface_service}.
 *
 * @author josesuero
 */
@RegisterMapper(Interface_ServiceDao.Mapper.class)
@UseStringTemplate3StatementLocator
public abstract class Interface_ServiceDao {

	/**
	 *
	 */
	static public class Mapper implements ResultSetMapper<Interface_Service> {

		private static final String ID = "id";
		private static final String ID_INTERFACE = "id_interface";
		private static final String NAME = "name";
		private static final String CODE = "code";
		private static final String RETURNED_VALUES = "returned_values";
		private static final String ORDER_INDEX = "order_index";

		@Override
		public Interface_Service map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Interface_Service(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_INTERFACE) ? resultSet.getInt(ID_INTERFACE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(CODE) ? resultSet.getString(CODE) : "",
					hasColumn.containsKey(RETURNED_VALUES) ? resultSet.getString(RETURNED_VALUES) : "",
					hasColumn.containsKey(ORDER_INDEX) ? resultSet.getInt(ORDER_INDEX) : 0
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from interface_service <where> order by order_index, id")
	abstract public List<Interface_Service> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_interface
	 * @return
	 */
	@SqlQuery("select * from interface_service where id_interface = :id_interface order by order_index, id")
	abstract public List<Interface_Service> getAll(@Bind("id_interface") final int id_interface);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from interface_service where id = :id")
	abstract public Interface_Service get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into interface_service(id, id_interface, name, code, returned_values, order_index)"
			+ "	values(interface_service_seq.nextVal, :id_interface, :name, :code, :returned_values, :order_index)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Interface_Service item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update interface_service set "
			+ " id_interface = to_number(:id_interface), "
			+ " name = :name, "
			+ "	code = to_clob(:code), "
			+ " returned_values = :returned_values, "
			+ " order_index = to_number(:order_index) "
			+ "where id = :id")
	abstract public void update(@BindBean final Interface_Service item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from interface_service where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select sum(column_value) from table(sys.odcinumberlist("
			+ "	(select count(id) from workflow_step where id_type=3 and id_interface_service=:id)"
			+ ")) t")
	abstract public long dependentsCount(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	public boolean hasDependents(int id) {
		long count = dependentsCount(id);
		return count > 0;
	}
}
