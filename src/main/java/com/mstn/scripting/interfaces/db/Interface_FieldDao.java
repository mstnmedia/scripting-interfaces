/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.db;

import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code interface_field}.
 *
 * @author josesuero
 * @deprecated
 */
@RegisterMapper(Interface_FieldDao.Mapper.class)
@UseStringTemplate3StatementLocator
public interface Interface_FieldDao {

	/**
	 *
	 */
	static public class Mapper implements ResultSetMapper<Interface_Field> {

		private static final String ID = "id";
		private static final String ID_INTERFACE = "id_interface";
		private static final String NAME = "name";
		private static final String LABEL = "label";
		private static final String ID_TYPE = "id_type";
		private static final String ORDER_INDEX = "order_index";
		private static final String REQUIRED = "required";
		private static final String READONLY = "readonly";
		private static final String DEFAULT_VALUE = "default_value";
		private static final String GRID = "grid";
		private static final String SOURCE = "source";
		private static final String ONVALIDATE = "onvalidate";
		private static final String ONCHANGE = "onchange";
		private static final String ONFOCUS = "onfocus";
		private static final String ONBLUR = "onblur";

		@Override
		public Interface_Field map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Interface_Field(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_INTERFACE) ? resultSet.getInt(ID_INTERFACE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(LABEL) ? resultSet.getString(LABEL) : "",
					hasColumn.containsKey(ID_TYPE) ? resultSet.getInt(ID_TYPE) : 0,
					hasColumn.containsKey(ORDER_INDEX) ? resultSet.getInt(ORDER_INDEX) : 0,
					hasColumn.containsKey(REQUIRED) ? resultSet.getBoolean(REQUIRED) : false,
					hasColumn.containsKey(READONLY) ? resultSet.getBoolean(READONLY) : false,
					hasColumn.containsKey(DEFAULT_VALUE) ? resultSet.getString(DEFAULT_VALUE) : "",
					hasColumn.containsKey(GRID) ? resultSet.getString(GRID) : "",
					hasColumn.containsKey(SOURCE) ? resultSet.getString(SOURCE) : "",
					hasColumn.containsKey(ONVALIDATE) ? resultSet.getString(ONVALIDATE) : "",
					hasColumn.containsKey(ONCHANGE) ? resultSet.getString(ONCHANGE) : "",
					hasColumn.containsKey(ONFOCUS) ? resultSet.getString(ONFOCUS) : "",
					hasColumn.containsKey(ONBLUR) ? resultSet.getString(ONBLUR) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from interface_field <where> order by order_index, label, id")
	List<Interface_Field> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_interface
	 * @return
	 */
	@SqlQuery("select * from interface_field where id_interface = :id_interface order by order_index, label, id")
	List<Interface_Field> getAll(@Bind("id_interface") final int id_interface);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from interface_field where id = :id")
	Interface_Field get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into interface_field(id, id_interface, name, label, id_type, order_index, required, readonly, default_value, grid, source, onvalidate, onchange, onfocus, onblur)"
			+ "	values(interface_field_seq.nextVal, :id_interface, :name, :label, :id_type, :order_index, :required, :readonly, :default_value, :grid, :source, :onvalidate, :onchange, :onfocus, :onblur)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Interface_Field item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update interface_field set"
			+ " id_interface = to_number(:id_interface),"
			+ " name = :name,"
			+ " label = :label,"
			+ " id_type = to_char(:id_type),"
			+ " order_index = to_number(:order_index),"
			+ " required = to_number(:required),"
			+ " readonly = to_number(:readonly),"
			+ " default_value = :default_value,"
			+ " grid = :grid,"
			+ " source = :source,"
			+ " onvalidate = :onvalidate,"
			+ " onchange = :onchange,"
			+ " onfocus = :onfocus,"
			+ " onblur = :onblur "
			+ "where id = :id")
	void update(@BindBean final Interface_Field item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from interface_field where id = :id")
	int delete(@Bind("id") final int id);
}
