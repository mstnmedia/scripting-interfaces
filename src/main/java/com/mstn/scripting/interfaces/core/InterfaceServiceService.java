/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.core;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.core.models.ScriptingInterfacesConfiguration;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.db.Interface_ServiceDao;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import org.apache.http.client.HttpClient;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con los servicios de interfaces.
 *
 * @author amatos
 */
public abstract class InterfaceServiceService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public InterfaceServiceService() {
	}
	private ScriptingInterfacesConfiguration config;

	/**
	 *
	 * @return
	 */
	public ScriptingInterfacesConfiguration getConfig() {
		return config;
	}

	/**
	 *
	 * @param config
	 * @return
	 */
	public InterfaceServiceService setConfig(ScriptingInterfacesConfiguration config) {
		this.config = config;
		return this;
	}

	private HttpClient httpClient;

	/**
	 *
	 * @return
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public InterfaceServiceService setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}

	private InterfaceBaseService interfaceBaseService;

	/**
	 *
	 * @param interfaceBaseService
	 * @return
	 */
	public InterfaceServiceService setInterfaceBaseService(InterfaceBaseService interfaceBaseService) {
		this.interfaceBaseService = interfaceBaseService;
		return this;
	}

	@CreateSqlObject
	abstract Interface_ServiceDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Interface_Service> getAll(WhereClause where) {
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param loadInterfaces
	 * @return
	 */
	public List<Interface_Service> getAll(WhereClause where, boolean loadInterfaces) {
		List<Interface_Service> list = getAll(where);
		if (loadInterfaces) {
			for (int i = 0; i < list.size(); i++) {
				Interface_Service service = list.get(i);
				Interface inter = interfaceBaseService.get(service.getId_interface());
				service.setInterface(inter);
			}
		}
		return list;
	}

	/**
	 *
	 * @param id_interface
	 * @return
	 */
	public List<Interface_Service> getAll(int id_interface) {
		return dao().getAll(id_interface);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Interface_Service getOrNull(int id) {
		return dao().get(id);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Interface_Service get(int id) {
		Interface_Service item = getOrNull(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param item
	 */
	public void addErrorToValues(Interface_Service item) {
		item.setReturned_values(addErrorToValues(item.getReturned_values()));
	}

	/**
	 *
	 * @param values
	 * @return
	 */
	public String addErrorToValues(String values) {
		List<String> valueList = Utils.splitAsList(values);
		HashMap<String, Boolean> map = new HashMap<>();
		valueList.forEach(value -> map.put(value, true));
		map.put(InterfaceOptions.ERROR, true);
		return String.join(",", map.keySet());
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	@Transaction
	public Interface_Service create(Interface_Service item, User user) {
		int id = 0;
		try {
			addErrorToValues(item);
			id = dao().insert(item);
			Interface_Service after = get(id);
			log().insert(user.getId(), "Servicios de interfaces", id, "Crear", item, after, user.getIp());
			return after;
		} catch (Exception ex) {
			log().insert(user.getId(), "Servicios de interfaces", id, "Error creando", item, ex, user.getIp());
			throw ex;
		}
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public Interface_Service update(Interface_Service item, User user) throws Exception {
		try {
			Interface_Service before = get(item.getId());
			addErrorToValues(item);
			dao().update(item);
			Interface_Service after = get(item.getId());
			// TODO: Update dependents workflow_steps
			String updatedSteps = Workflow_Step.updateInterfaceServiceReferers(after, httpClient, config.getApiURL(), user.getToken());
			if (!"true".equals(updatedSteps)) {
				throw new Exception("Dependent steps can't be updated: " + updatedSteps);
			}
			log().insert(user.getId(), "Servicios de interfaces", before.getId(), "Actualizar", before, after, user.getIp());
			return after;
		} catch (Exception ex) {
			log().insert(user.getId(), "Servicios de interfaces", item.getId(), "Error actualizando", item, ex, user.getIp());
			throw ex;
		}
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	@Transaction
	public String delete(final int id, User user) {
		Interface_Service before = null;
		try {
			before = get(id);
			boolean hasDependents = dao().hasDependents(id);
			int result = hasDependents ? -1 : dao().delete(id);
			switch (result) {
				case 1:
					log().insert(user.getId(), "Servicios de interfaces", before.getId(), "Eliminar", before, null, user.getIp());
					return SUCCESS;
				case 0:
					throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
				case -1:
					throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Status.PRECONDITION_FAILED);
				default:
					throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception ex) {
			log().insert(user.getId(), "Servicios de interfaces", id, "Error eliminando", before, ex, user.getIp());
			throw ex;
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
