/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.core;

import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.interfaces.db.InterfaceDao;
import com.mstn.scripting.core.models.Interface_Option;
import com.mstn.scripting.core.models.ScriptingInterfacesConfiguration;
import com.mstn.scripting.interfaces.db.Interface_OptionDao;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import org.apache.http.client.HttpClient;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las opciones de interfaces.
 *
 * @author amatos
 * @deprecated 
 */
public abstract class InterfaceOptionService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	private ScriptingInterfacesConfiguration config;
	private HttpClient httpClient;

	/**
	 *
	 */
	public InterfaceOptionService() {
	}

	/**
	 *
	 * @return
	 */
	public ScriptingInterfacesConfiguration getConfig() {
		return config;
	}

	/**
	 *
	 * @param config
	 * @return
	 */
	public InterfaceOptionService setConfig(ScriptingInterfacesConfiguration config) {
		this.config = config;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public InterfaceOptionService setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}

	@CreateSqlObject
	abstract Interface_OptionDao dao();

	@CreateSqlObject
	abstract InterfaceDao interfaceDao();

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Interface_Option> getAll(WhereClause where) {
		return dao().getAll(where.preparedString, where);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Interface_Option get(int id) {
		Interface_Option item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}

		return item;
	}

	/**
	 *
	 * @param item
	 * @return
	 */
	public Interface_Option create(Interface_Option item) {
		int id = dao().insert(item);

		Interface_Option actualOption = dao().get(id);
//		Interface Interface = interfaceDao().get(actualOption.getId_interface());

//		//TODO: Investigar si esto debe afectar a versiones las archivadas, publicada y editable
//		List<Workflow_Step> steps = getStepsReferersOfInterface(Interface.getId(), true);
//		for (int i = 0; i < steps.size(); i++) {
//			Workflow_Step step = steps.get(i);
//			List<Workflow_Step_Option> options = step.getWorkflow_step_option();
//			boolean matches = false;
//			for (Workflow_Step_Option option : options) {
//				if (option.getName().equals(actualOption.getValue())) {
//					matches = true;
//					break;
//				}
//			}
//			if (!matches) {
//				Workflow_Step_Option newOption = new Workflow_Step_Option(
//						0, step.getId_center(), step.getId_workflow(), step.getId_workflow_version(),
//						step.getId(), step.getGuid(), 0, actualOption.getValue(),
//						actualOption.getColor(), "", actualOption.getOrder_index()
//				);
//				workflow_Step_OptionDao().insert(newOption);
//			}
//		}
		return actualOption;
	}

	/**
	 *
	 * @param item
	 * @return
	 */
	public Interface_Option update(Interface_Option item) {
		Interface_Option previousOption = get(item.getId());
		dao().update(item);

		Interface_Option actualOption = dao().get(item.getId());
//		Interface Interface = interfaceDao().get(actualOption.getId_interface());

//		//TODO: Investigar si esto debe afectar a versiones las archivadas, publicada y editable
//		List<Workflow_Step> steps = getStepsReferersOfInterface(Interface.getId(), true);
//		for (int i = 0; i < steps.size(); i++) {
//			Workflow_Step step = steps.get(i);
//			List<Workflow_Step_Option> options = step.getWorkflow_step_option();
//
//			for (Workflow_Step_Option option : options) {
//				if (option.getName().equals(previousOption.getValue())) {
//					workflow_Step_OptionDao().delete(option.getId());
//					break;
//				}
//			}
//			workflow_Step_OptionDao().insert(new Workflow_Step_Option(
//					0, step.getId_center(), step.getId_workflow(), step.getId_workflow_version(),
//					step.getId(), step.getGuid(), 0, actualOption.getValue(),
//					actualOption.getColor(), "", actualOption.getOrder_index()
//			));
//		}
		return actualOption;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public String delete(final int id) {
//		Interface_Option previousOption = dao().get(id);
		int result = dao().delete(id);
		switch (result) {
			case 1: {
//				//TODO: Investigar si esto debe afectar a versiones las archivadas, publicada y editable
//				Interface Interface = interfaceDao().get(previousOption.getId_interface());
//				List<Workflow_Step> steps = getStepsReferersOfInterface(Interface.getId(), true);
//				for (int i = 0; i < steps.size(); i++) {
//					Workflow_Step step = steps.get(i);
//					List<Workflow_Step_Option> options = step.getWorkflow_step_option();
//
//					for (Workflow_Step_Option option : options) {
//						if (option.getName().equals(previousOption.getValue())) {
//							workflow_Step_OptionDao().delete(option.getId());
//							break;
//						}
//					}
//				}

				return SUCCESS;
			}
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

//	public List<Workflow_Step> getStepsReferersOfInterface(int id_interface, boolean loadOptions, String bearerToken) throws IOException {
//		String strPayload = JSON.convertToString(new IdValueParams(id_interface, Boolean.toString(loadOptions)));
//		String strResponse = HTTP.postResource(
//				httpClient,
//				config.getApiURL() + "/workflows/workflowstep/getReferersOfInterface",
//				strPayload,
//				bearerToken
//		);
//		return Arrays.asList(JSON.toObject(strResponse, Workflow_Step[].class));
//	}
}
