/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.core;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnector;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.core.models.Workflow_Step;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;

/**
 * Clase para interactuar con las interfaces estáticas.
 *
 * @author amatos
 */
public class InterfaceService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

//	private ScriptingInterfacesConfiguration config;
//	private HttpClient httpClient;
	private InterfaceServiceService interfaceServiceService;
	private List<InterfaceConnectorBase> connectors;
	private HashMap<Integer, InterfaceConnectorBase> mappedConnectors;

	/**
	 *
	 */
	public InterfaceService() {
	}

//	public InterfaceService setConfig(ScriptingInterfacesConfiguration config) {
//		this.config = config;
//		return this;
//	}
//
//	public InterfaceService setInterfaceBaseService(HttpClient httpClient) {
//		this.httpClient = httpClient;
//		return this;
//	}

	/**
	 *
	 * @param interfaceServiceService
	 * @return
	 */
	public InterfaceService setInterfaceServiceService(InterfaceServiceService interfaceServiceService) {
		this.interfaceServiceService = interfaceServiceService;
		return this;
	}

	/**
	 *
	 * @param connectors
	 * @return
	 */
	public InterfaceService setConnectors(List<InterfaceConnectorBase> connectors) {
		this.connectors = connectors;
		this.connectors.sort((c1, c2) -> {
			return c1.getLABEL().compareTo(c2.getLABEL());
		});

		this.mappedConnectors = new HashMap<>();
		connectors.forEach(connector -> {
			mappedConnectors.put(connector.getInterface().getId(), connector);
		});
		return this;
	}

//	@CreateSqlObject
//	abstract InterfaceDao dao();
//
//	@CreateSqlObject
//	abstract Interface_OptionDao optionDao();
//
//	@CreateSqlObject
//	abstract Interface_FieldDao fieldDao();

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	public List<Interface> getAll() throws Exception {
		return getAll(false);
	}

	/**
	 *
	 * @param loadChildren
	 * @return
	 * @throws Exception
	 */
	public List<Interface> getAll(boolean loadChildren) throws Exception {
		List<Interface> list = new ArrayList<>();
		for (int i = 0; i < connectors.size(); i++) {
			InterfaceConnectorBase connector = connectors.get(i);
			if (loadChildren) {
				Interface inter = connector.getInterfaceWithChildren();
				inter.setInterface_services(getServices(inter.getId()));
				list.add(inter);
			}
			list.add(connector.getInterface());
		}
		return list;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Interface get(int id) {
		InterfaceConnectorBase connector = getConnector(id);
		return connector.getInterface();
	}

	/**
	 *
	 * @param id
	 * @param loadChildren
	 * @return
	 * @throws Exception
	 */
	public Interface get(int id, boolean loadChildren) throws Exception {
		InterfaceConnectorBase connector = getConnector(id);
		if (loadChildren) {
			Interface inter = connector.getInterfaceWithChildren();
			inter.setInterface_services(getServices(inter.getId()));
			return inter;
		}
		return get(id);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public InterfaceConnectorBase getConnector(int id) {
		InterfaceConnectorBase connector = mappedConnectors.get(id);
		if (Objects.isNull(connector)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return connector;
	}

	/**
	 *
	 * @param id_interface
	 * @return
	 */
	public List<Interface_Service> getServices(int id_interface) {
		return interfaceServiceService.getAll(id_interface);
	}

	/**
	 *
	 * @param interfaceID
	 * @param payload
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public TransactionInterfacesResponse getForm(int interfaceID, TransactionInterfacesPayload payload, User user) throws Exception {
		try {
			Class<? extends InterfaceConnector> connectorClass = getConnector(interfaceID).getClass();
			InterfaceConnector connector = connectorClass.newInstance();
			Workflow_Step step = payload.getWorkflow_step();
			if (step != null) {
				Interface_Service service = interfaceServiceService.getOrNull(step.getId_interface_service());
				payload.setInterface_service(service);
			}
			TransactionInterfacesResponse response = connector.getForm(payload, user);
			return response;
		} catch (Exception ex) {
			return InterfaceConnectorBase.setAsExceptionResponse(
					this.getClass(),
					new TransactionInterfacesResponse(),
					ex
			);
		}
	}

	//public Interface create(Interface item) {
	//	int id = dao().insert(item);
	//
	//	return get(id);
	//}
	//public Interface update(Interface item) {
	//	if (Objects.isNull(dao().get(item.getId()))) {
	//		throw new WebApplicationException(String.format(NOT_FOUND, item.getId()), Status.NOT_FOUND);
	//	}
	//	//TODO: Si se edita una interfaz que se usa en un flujo ya publicado, pueden quedar transacciones sin camino para avanzar
	//	dao().update(item);
	//	return get(item.getId());
	//}
	//
	//public String delete(final int id) {
	//	int result = dao().delete(id);
	//	switch (result) {
	//		case 1:
	//			return SUCCESS;
	//		case 0:
	//			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
	//		default:
	//			throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
	//	}
	//}
	//
	//public String performHealthCheck() {
	//	try {
	//		dao().get(0);
	//	} catch (UnableToObtainConnectionException ex) {
	//		return checkUnableToObtainConnectionException(ex);
	//	} catch (UnableToExecuteStatementException ex) {
	//		return checkUnableToExecuteStatementException(ex);
	//	} catch (Exception ex) {
	//		return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	//	}
	//	return null;
	//}
	//private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
	//	if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
	//		return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
	//	} else if (ex.getCause() instanceof java.sql.SQLException) {
	//		return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
	//	} else {
	//		return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	//	}
	//}
	//
	//private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
	//	if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
	//		return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
	//	} else {
	//		return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	//	}
	//}
}
