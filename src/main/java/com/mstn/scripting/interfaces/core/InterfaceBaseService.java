/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.core;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
//import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Interface_Result;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.db.InterfaceDao;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las interfaces dinámicas.
 *
 * @author amatos
 */
public abstract class InterfaceBaseService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

//	private ScriptingInterfacesConfiguration config;
//	private HttpClient httpClient;
	private InterfaceServiceService serviceService;
	private InterfaceResultService resultService;
	private List<Interface_Result> interfaceResults;
//	private final HashMap<String, Interface_Result> mapInterfaceResults = new HashMap();
//	private InterfaceFieldService fieldService;
//	private List<Interface_Field> interfaceFields;
//	private final HashMap<String, Interface_Field> mapInterfaceFields = new HashMap();
	private List<InterfaceBase> interfaceBases;
	private final HashMap<Integer, Interface> mapInterfaces = new HashMap();
	private final HashMap<String, InterfaceBase> mapInterfaceBases = new HashMap();

	/**
	 *
	 */
	public InterfaceBaseService() {
	}

	/**
	 *
	 * @param interfaceServiceService
	 * @return
	 */
	public InterfaceBaseService setInterfaceServiceService(InterfaceServiceService interfaceServiceService) {
		this.serviceService = interfaceServiceService;
		this.serviceService.setInterfaceBaseService(this);
		return this;
	}

	/**
	 *
	 * @param interfaceResultService
	 * @return
	 */
	public InterfaceBaseService setInterfaceResultService(InterfaceResultService interfaceResultService) {
		this.resultService = interfaceResultService;
		return this;
	}

	/**
	 *
	 * @param bases
	 * @return
	 * @throws Exception
	 */
	public InterfaceBaseService setInterfaceBases(List<InterfaceBase> bases) throws Exception {
		this.interfaceBases = bases;

		WhereClause whereInterfaces = new WhereClause();
		List<Interface> dbInterfaces = dao().getAll(whereInterfaces.getPreparedString(), whereInterfaces);
		HashMap<String, Interface> mapCurrentInterfaces = new HashMap();
		for (int i = 0; i < dbInterfaces.size(); i++) {
			Interface inter = dbInterfaces.get(i);
			mapCurrentInterfaces.put(inter.getName(), inter);
		}

		WhereClause whereResults = new WhereClause();
		List<Interface_Result> dbResults = resultService.getAll(whereResults);
		HashMap<String, Interface_Result> mapCurrentResults = new HashMap();
		for (int i = 0; i < dbResults.size(); i++) {
			Interface_Result result = dbResults.get(i);
			mapCurrentResults.put(result.getName(), result);
		}

		for (int i = 0; i < bases.size(); i++) {
			InterfaceBase base = bases.get(i);
			String baseName = base.getNAME();
			InterfaceConfigs.log(this.getClass(), "BaseName: " + baseName);
			mapInterfaceBases.put(baseName, base);
			List<Interface> interfaces = base.getInterfaces(true);
			for (int j = 0; j < interfaces.size(); j++) {
				Interface inter = interfaces.get(j);
				InterfaceConfigs.log(this.getClass(), "InterName: " + inter.getName());
				if (mapCurrentInterfaces.containsKey(inter.getName())) {
					Interface dbInter = mapCurrentInterfaces.get(inter.getName());
					inter.setId(dbInter.getId());
					String label = Utils.coalesce(dbInter.getLabel(), inter.getLabel());
					inter.setLabel(label);
					inter.setDeleted(dbInter.getDeleted());
					//inter.setServer_url(dbInter.getServer_url()); //Para que lo tome cada una desde el archivo de configuración.
					inter.setValue_step(dbInter.getValue_step());
				} else {
					int id = dao().insert(inter);
					inter.setId(id);
					Interface_Service service = new Interface_Service(0, inter.getId(), "Predeterminado", "return true;", "true,error");
					serviceService.create(service, User.SYSTEM);
					mapCurrentInterfaces.put(inter.getName(), inter);
				}
				mapInterfaces.put(inter.getId(), inter);

				List<Transaction_Result> results = inter.getInterface_results();
				for (int k = 0; k < results.size(); k++) {
					Transaction_Result tResult = results.get(k);
					Utils.logFinest(this.getClass(), "InterResult: " + tResult.getName());
					if (mapCurrentResults.containsKey(tResult.getName())) {
						Interface_Result dbResult = mapCurrentResults.get(tResult.getName());
						tResult.setId(dbResult.getId());
						tResult.setLabel(Utils.coalesce(dbResult.getLabel(), tResult.getLabel()));
						tResult.setProps(dbResult.getProps());
					} else {
						Interface_Result newResult = resultService.create(new Interface_Result(inter.getId(), inter.getId(), tResult.getName(), tResult.getLabel(), tResult.getProps()));
						tResult.setId(newResult.getId());
						mapCurrentResults.put(newResult.getName(), newResult);
					}
				}
			}
		}
		return this;
	}

	@CreateSqlObject
	abstract InterfaceDao dao();

	@CreateSqlObject
	abstract LogDao log();
//
//	@CreateSqlObject
//	abstract Interface_OptionDao optionDao();
//
//	@CreateSqlObject
//	abstract Interface_FieldDao fieldDao();

	/**
	 *
	 * @return @throws Exception
	 */
	public List<Interface> getAll() throws Exception {
		return getAll(false);
	}

	/**
	 *
	 * @param loadChildren
	 * @return
	 * @throws Exception
	 */
	public List<Interface> getAll(boolean loadChildren) throws Exception {
		return mapInterfaces.values()
				.stream()
				.map((item) -> fromAnother(item, loadChildren))
				.sorted((a, b) -> Integer.compare(a.getId(), b.getId()))
				.collect(Collectors.toList());
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Interface get(int id) {
		return get(id, false);
	}

	/**
	 *
	 * @param id
	 * @param loadChildren
	 * @return
	 */
	public Interface get(int id, boolean loadChildren) {
		Interface item = mapInterfaces.get(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), HttpStatus.NOT_FOUND_404);
		}
		return fromAnother(item, loadChildren);
	}

	Interface fromAnother(Interface item, boolean loadChildren) {
		Interface inter = new Interface(item.getId(), item.getName(), item.getLabel(), item.getServer_url(), item.getValue_step(), item.isKeep_results(), item.getDeleted());
		if (loadChildren) {
			inter.setInterface_field(item.getInterface_field());
			inter.setInterface_results(item.getInterface_results());
			loadServices(inter);
		}
		return inter;
	}

	/**
	 *
	 * @param item
	 * @return
	 */
	public List<Interface_Service> loadServices(Interface item) {
		List<Interface_Service> list = serviceService.getAll(item.getId());
		item.setInterface_services(list);
		return list;
	}

	/**
	 *
	 * @param interfaceID
	 * @param payload
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public TransactionInterfacesResponse exec(int interfaceID, TransactionInterfacesPayload payload, User user) throws Exception {
		try {
			Workflow_Step step = payload.getWorkflow_step();
			if (step != null) {
				Interface_Service service = serviceService.getOrNull(step.getId_interface_service());
				payload.setInterface_service(service);
			}
			Interface inter = mapInterfaces.get(interfaceID);
			String[] nameParts = inter.getName().split(InterfaceBase.SEPARATOR, 2);
			InterfaceBase mappedBase = mapInterfaceBases.get(nameParts[0]);
			InterfaceBase base = mappedBase.getClass().newInstance();
			TransactionInterfacesResponse response = base.callMethod(inter, payload, user);
			return response;
		} catch (Exception ex) {
			Utils.logException(InterfaceBaseService.class, ex.getMessage(), ex);
			return TransactionInterfacesResponse.fromException(new TransactionInterfacesResponse(), ex, user);
		}
	}

	//public Interface create(Interface item) {
	//	int id = dao().insert(item);
	//
	//	return get(id);
	//}
	//public Interface update(Interface item) {
	//	if (Objects.isNull(dao().get(item.getId()))) {
	//		throw new WebApplicationException(String.format(NOT_FOUND, item.getId()), Status.NOT_FOUND);
	//	}
	//	//TODO: Si se edita una interfaz que se usa en un flujo ya publicado, pueden quedar transacciones sin camino para avanzar
	//	dao().update(item);
	//	return get(item.getId());
	//}
	//
	//public String delete(final int id) {
	//	int result = dao().delete(id);
	//	switch (result) {
	//		case 1:
	//			return SUCCESS;
	//		case 0:
	//			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
	//		default:
	//			throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
	//	}
	//}
	//
	//public String performHealthCheck() {
	//	try {
	//		dao().get(0);
	//	} catch (UnableToObtainConnectionException ex) {
	//		return checkUnableToObtainConnectionException(ex);
	//	} catch (UnableToExecuteStatementException ex) {
	//		return checkUnableToExecuteStatementException(ex);
	//	} catch (Exception ex) {
	//		return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	//	}
	//	return null;
	//}
	//private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
	//	if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
	//		return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
	//	} else if (ex.getCause() instanceof java.sql.SQLException) {
	//		return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
	//	} else {
	//		return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	//	}
	//}
	//
	//private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
	//	if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
	//		return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
	//	} else {
	//		return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	//	}
	//}
}
