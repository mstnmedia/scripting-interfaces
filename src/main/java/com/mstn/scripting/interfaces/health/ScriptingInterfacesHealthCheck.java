/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.health;

import com.codahale.metrics.health.HealthCheck;
import com.mstn.scripting.interfaces.core.InterfaceService;

/**
 * Clase que se registra para consultar el estado del microservicio.
 *
 * @author josesuero
 */
public class ScriptingInterfacesHealthCheck extends HealthCheck {

	private static final String HEALTHY = "The Interface Service is healthy for read and write";
	private static final String UNHEALTHY = "The Interface Service is not healthy. ";
	private static final String MESSAGE_PLACEHOLDER = "{}";

	private final InterfaceService interfaceService;

	/**
	 *
	 * @param interfaceService
	 */
	public ScriptingInterfacesHealthCheck(InterfaceService interfaceService) {
		this.interfaceService = interfaceService;
	}

	@Override
	protected Result check() throws Exception {
		String dbHealthStatus = null; //interfaceService.performHealthCheck();

		if (dbHealthStatus == null) {
			return Result.healthy(HEALTHY);
		} else {
			return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, dbHealthStatus);
		}
	}
}
